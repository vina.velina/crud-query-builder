<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MahasiswaController extends Controller
{
    
    public function home(){
        return view('master.app');
    }
    public function index(){
        $data_mahasiswa = DB::table('mahasiswa')->get();
        $id = 2;
        return view('page.data-mahasiswa.index',compact('data_mahasiswa','id'));
    }
    public function create(){
        return view('page.data-mahasiswa.create');
    }
    public function store(Request $request){
        //dd($request->all()); untuk vardump  
       DB::table('mahasiswa')->insert([
            'nim'=> $request ->nim,
            'kelas' => $request->kelas,
            'nama'=> $request->nama_mahasiswa,
            'prodi'=> $request->prodi,
            'fakultas'=> $request->fakultas,
        ]);
        return redirect(route('mahasiswa'));
    }
    public function edit($id){
        $data_mahasiswa_single= DB::table('mahasiswa')->where('id',$id)->first();
        if($data_mahasiswa_single==null){
            abort(404);
        }
        return view('page.data-mahasiswa.edit',['data_mahasiswa'=>$data_mahasiswa_single,'id' => 2]);
    }
    public function update(Request $request,$id){
        DB::table('mahasiswa')->where('id',$id)->update([
            'nim'=> $request ->nim,
            'kelas' => $request->kelas,
            'nama'=> $request->nama_mahasiswa,
            'prodi'=> $request->prodi,
            'fakultas'=> $request->fakultas,
        ]);
        return redirect(route('mahasiswa'));
    }
    public function delete($id){
        DB::table('mahasiswa')->where('id',$id)->delete();
        return redirect(route('mahasiswa'));
    }

}
