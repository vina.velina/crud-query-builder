<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::prefix('mahasiswa')->group(function () {
    Route::get('/', [App\Http\Controllers\MahasiswaController::class, 'index'])->name('mahasiswa');
    Route::get('tambah data', [App\Http\Controllers\MahasiswaController::class, 'create'])->name('mahasiswa.tambah');
    Route::get('edit/{id}', [App\Http\Controllers\MahasiswaController::class, 'edit'])->name('mahasiswa.edit');
    Route::post('aksi', [App\Http\Controllers\MahasiswaController::class, 'store'])->name('mahasiswa.aksi');
    Route::patch('update/{id}', [App\Http\Controllers\MahasiswaController::class, 'update'])->name('mahasiswa.update');
    Route::delete('delete/{id}', [App\Http\Controllers\MahasiswaController::class, 'delete'])->name('mahasiswa.delete');
});
