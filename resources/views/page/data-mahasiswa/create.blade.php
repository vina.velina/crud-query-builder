@extends('master.app')
@section('title', ' Tambah Data Mahasiswa')

@section('content')
<div class="jumbotron">
    <h3>Tambah Data Mahasiswa</h3>
    <hr class="my-4">
    <form action="{{ route("mahasiswa.aksi") }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="inputName">NIM</label>
            <input type="number" min="0" maxlength="10" class="form-control" id="inputName" required name="nim">
        </div>
        <div class="form-group">
            <label for="inputNamaMahasiswa">Nama</label>
            <input type="text" class="form-control" id="inputNamaMahasiswa" required
             name="nama_mahasiswa">
        </div>
        <div class="form-group">
            <label for="inputKelas">Kelas</label>
            <input type="text" class="form-control" id="inputKelas" required name="kelas">
        </div>
        <div class="form-group">
            <label for="inputProdi">Prodi</label>
            <select name="prodi" id="inputProdi" required class="form-control">
                <option value="">-- Pilih Prodi --</option>
                <option value="PTI">Pendidikan Teknik Informatika</option>
                <option value="SI">Sistem Informasi</option>
                <option value="MI">Manajemen Informatika</option>
                <option value="ILKOM">Ilmu Komputer</option>
            </select>
        </div>
        <div class="form-group">
            <label for="inputFakultas">Fakultas</label>
            <select class="form-control" required name="fakultas" id="fakultas">
                <option value="">-- Pilih Fakultas --</option>
                <option value="FTK">Fakultas Teknik dan Kejuruan</option>
                <option value="Lainnya">Lainnya</option>
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

  </div>
@endsection