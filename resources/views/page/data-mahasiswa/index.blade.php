@extends('master.app')
@section('title', 'Data Mahasiswa')

@section('content')
<div class="jumbotron">
    <h3>Data Mahasiswa</h3>
    <hr class="my-4">
    <a href="{{ route('mahasiswa.tambah') }}" class="btn btn-primary btn-sm mb-4">Tambah Data</a>
    <table class="table">
        <thead class="thead-dark">
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nim Mahasiswa</th>
            <th scope="col">Nama Mahasiswa</th>
            <th scope="col">Kelas Mahasiswa</th>
            <th scope="col">Prodi Mahasiswa</th>
            <th scope="col">Fakultas Mahasiswa</th>
            <th scope="col">Aksi</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($data_mahasiswa as $key=>$mahasiswa)
                
          <tr>
            <th scope="row">{{ $key+1 }}</th>
            <td>{{ $mahasiswa->nim }}</td>
            <td>{{ $mahasiswa->nama }}</td>
            <td>{{ $mahasiswa->kelas }}</td>
            <td>{{ $mahasiswa->prodi }}</td>
            <td>{{ $mahasiswa->fakultas }}</td>
            <td>
              <div class="row">
                <div class="col-6">
                  <a href="{{ route('mahasiswa.edit',$mahasiswa->id) }}" class="btn btn-warning btn-sm mb-4">Ubah</a>
                </div>
                <div class="col-6">
                  <form action="{{ route('mahasiswa.delete',$mahasiswa->id) }}" onsubmit="return confirm('Apakah Yakin Ingin Dihapus?');"  method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger btn-sm">Hapus</button>
                  </form>
                </div>
              </div>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
  </div>
@endsection