@extends('master.app')
@section('title', ' Edit Data Mahasiswa')

@section('content')
<div class="jumbotron">
    <h3>Edit Data Mahasiswa</h3>
    <hr class="my-4">
    <form action="{{ route("mahasiswa.update",$data_mahasiswa->id) }}" method="POST">
        @csrf
        @method('PATCH')
        <div class="form-group">
            <label for="inputName">NIM</label>
            <input type="number" min="0" maxlength="10" class="form-control" id="inputName" name="nim" value="{{ $data_mahasiswa->nim }}">
        </div>
        <div class="form-group">
            <label for="inputNamaMahasiswa">Nama</label>
            <input type="text" class="form-control" id="inputNamaMahasiswa" name="nama_mahasiswa"  value="{{ $data_mahasiswa->nama }}">
        </div>
        <div class="form-group">
            <label for="inputKelas">Kelas</label>
            <input type="text" class="form-control" value="{{ $data_mahasiswa->kelas }}" id="inputKelas" required name="kelas">
        </div>
        <div class="form-group">
            <label for="inputProdi">Prodi</label>
            <select name="prodi" id="inputProdi" required class="form-control">
                <option value="">-- Pilih Prodi --</option>
                <option value="PTI" {{ $data_mahasiswa->prodi == "PTI" ? "selected":"" }}>Pendidikan Teknik Informatika</option>
                <option value="SI" {{ $data_mahasiswa->prodi == "SI" ? "selected":"" }}>Sistem Informasi</option>
                <option value="MI" {{ $data_mahasiswa->prodi == "MI" ? "selected":"" }}>Manajemen Informatika</option>
                <option value="ILKOM" {{ $data_mahasiswa->prodi == "ILKOM" ? "selected":"" }}>Ilmu Komputer</option>
            </select>
        </div>
        <div class="form-group">
            <label for="inputFakultas">Fakultas</label>
            <select class="form-control" required name="fakultas" id="fakultas">
                <option value="">-- Pilih Fakultas --</option>
                <option value="FTK" {{ $data_mahasiswa->fakultas == "FTK" ? "selected":"" }}>Fakultas Teknik dan Kejuruan</option>
                <option value="Lainnya" {{ $data_mahasiswa->fakultas == "Lainnya" ? "selected":"" }}>Lainnya</option>
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

  </div>
    
@endsection