@extends('master.app')
@section('title', 'Data Mahasiswa')

@section('content')
<div class="jumbotron">
    <h1 class="display-4">Selamat Datang!</h1>
    <p class="lead">Sistem Informasi Manajemen Mahasiswa</p>
    <hr class="my-4">
    <p>Selamat menggunakan sistem informasi ini, silahkan melakukan manajemen mahasiswa pada menu mahasiswa</p>
    <a class="btn btn-primary btn-md" href="{{ route('mahasiswa') }}" role="button">Mahasiswa</a>
  </div>
@endsection